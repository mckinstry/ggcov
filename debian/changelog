ggcov (0.9-22) UNRELEASED; urgency=medium

  * Standards-Version: 4.3.0
  * Update version recognition for gcc-8* from upstream
  * Re-introduce libgnomeui, libglade-dev for final GTK2 builds
  * db-5.3.patch: Use latest MariaDB

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 12 Jan 2019 18:26:47 +0000

ggcov (0.9-21) unstable; urgency=medium

  * Standards-Version: 4.1.4
  * Set VCS to salsa.debian.org
  * Set Homepage to new github page
  * Drop B-D on libgnomeui, libglade-dev for GTK2 -> GTK3 transition
  * Disable gui for this upload. Closes: #885736
  * B-D on pkg-config, libglib2.0-dev, libxml-dev

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 15 May 2018 11:44:37 +0100

ggcov (0.9-20) unstable; urgency=medium

  * Standards-Version: 4.1.3
  * Drop obsolete --with autoconf
  * Support for gcc 7.3. Closes: #889127

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 02 Feb 2018 10:36:45 +0000

ggcov (0.9-19) unstable; urgency=medium

  * Push to unstable

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 18 Sep 2017 13:07:37 +0100

ggcov (0.9-18) experimental; urgency=medium

  * Add --disable-calltree, enabled automatically on non-x86 archs.
    This should enable a build on all archs.
  * gcc 7.2 support
  * Standards-Version: 4.1.0

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 17 Sep 2017 17:53:51 +0100

ggcov (0.9-17) experimental; urgency=medium

  * Experimental release, enabling all archs to see which fail following
    gcc-7 and fixes

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 17 Aug 2017 08:08:53 +0100

ggcov (0.9-16) unstable; urgency=medium

  * Patch to handle new gcc7 versioning system. Closes: #853417
  * Standards-Version: 4.0.1 now
  * dh-autoreconf, autotools-dev B-D now dropped.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 16 Aug 2017 12:05:47 +0100

ggcov (0.9-15) unstable; urgency=medium

  * Support for gcc 6.4; Closes: #868071
  * Standards-Version: 4.0.0; no changes required
  * DH_COMPAT=10 now

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 12 Jul 2017 13:38:58 +0100

ggcov (0.9-14) unstable; urgency=medium

  * Support for gcc 6.3. Closes: #8851002

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 12 Jan 2017 17:14:25 +0000

ggcov (0.9-13) unstable; urgency=medium

  * Fix for fpermissive error with g++ 6.2. Closes: #844884 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 21 Nov 2016 17:04:44 +0000

ggcov (0.9-12) unstable; urgency=medium

  * Support for gcc 6.2. Closes: #835728

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 25 Aug 2016 20:04:23 +0100

ggcov (0.9-11) unstable; urgency=medium

  * Fixes for gcc-6. Closes: #811766.
  * Add support for gcc 6.0, 6.1 compilers.
  * Standards-Version: 3.9.8
  * Update home page to ggcov.org

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 09 Jun 2016 09:30:55 +0100

ggcov (0.9-10) unstable; urgency=medium

  * Add support for gcc 5.3.  Closes: #809133

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 29 Dec 2015 00:10:40 +0000

ggcov (0.9-9) unstable; urgency=medium

   * Link against dl, needed for libbfd.a. Closes: #805199

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 20 Nov 2015 17:02:06 +0000

ggcov (0.9-8) unstable; urgency=medium

  * Add support for gcc 5.2. Closes: #797089. 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Aug 2015 12:31:25 +0100

ggcov (0.9-7) unstable; urgency=medium

  * Change new_gcc.patch to work with gcc 5.0,5.1. Closes: #777875.
  * Move to Standards-Version: 3.9.6
  * bash-completions move to new /usr/share/bash-completion/completions

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 25 Jun 2015 07:24:57 +0100

ggcov (0.9-6) unstable; urgency=medium

  "The Triskodecaphobian release"
  * Fix needed for 20140612 changes to libiberty-dev; add params.patch 
    to include definition removed from ansidecl.h
  * ggcov has a runtime dependency on libdatetime-perl for git-history-coverage 
  * Add bash completion for ggcov commands.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 14 Jun 2014 00:41:45 +0100

ggcov (0.9-5) unstable; urgency=medium

  * Add gcc4.9 support.  Closes: #751300.
  * Add libiberty-dev as B-D; add include so that it works correctly.
  * Re-enable automated tests now that test033 passes again :-)

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 12 Jun 2014 15:27:12 +0100

ggcov (0.9-4) unstable; urgency=medium

  * Minor patch for PATH_MAX needed for Hurd.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 08 Apr 2014 15:29:59 +0100

ggcov (0.9-3) unstable; urgency=medium

  * Re-add patches that were accidentally dropped in 0.9
  * Add Dep on libx11-dev for binutils-gold fix
  * Add gcc48.patch from Greg Banks for gcc-4.8 support.
  * Move dependency to gcc-4.8

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 08 Apr 2014 11:49:42 +0100

ggcov (0.9-2) unstable; urgency=medium

  * Incorporate copyright, control changes from Greg Banks.
    Includes dependency on gcc-4.7
   Closes LP: #713811. 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 04 Apr 2014 23:18:15 +0100

ggcov (0.9-1) unstable; urgency=medium

  * New upstream release. Closes: #684400.
  * Depend on autotools-dev to update config.*
  * Remove unused ldconfig calls in postinst file.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 24 Mar 2014 18:03:58 +0000

ggcov (0.8.4-3) unstable; urgency=medium

  * Standards-Version: 3.9.5
  * Add watch file.
  * Now use DH_COMPAT=9.
  * Support c++ extension. Closes: #684401
  * Now  depend on libdb-dev | libdb6.0-dev

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 08 Dec 2013 09:35:36 +0000

ggcov (0.8.4-2) unstable; urgency=low

  * Disable tests for amd64, as per Ubuntu.  Closes: #669099.
  * Depend on gcc-4.6, add note in README.Debian that this release does
    not work on gcc-4.7. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 30 Jun 2012 11:14:12 +0100

ggcov (0.8.4-1) unstable; urgency=low

  * New upstream release. Closes: #654256, #662713.
  * Move to Standards-Version: 3.9.3
  * Enable archs amd64, kfreebsd-amd64. Closes: #662714.
  * Tests: re-enable test 14, now test 029 fails (minor). bypassed.
  * Move to DH_COMPAT=8
  * Don't ship .la files in ggcov.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 12 Mar 2012 10:17:52 +0000

ggcov (0.8.3-1) unstable; urgency=low

  * New upstream release. Closes: #617479.
  * Disable test 028; seems broken in source.
  * Move to Standards-Version: 3.9.2 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 25 Apr 2011 00:31:39 +0100

ggcov (0.8.2-2) unstable; urgency=low

  * Fix FTBFS with binutils-gold. Patch thanks to  Bhavani Shankar.R .
    Closes: #608735. 
  * Move to Standards-Version: 3.9.1. 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 03 Jan 2011 20:15:43 +0000

ggcov (0.8.2-1) unstable; urgency=low

  * New upstream release. 
    Obsolete patches: 
      gcc44_fixes.patch, 
      compile_fixes.patch,
      ignore_tests.patch
  * Remove bashisms from debian/rules. Closes: #581477.
  * Standards-Version: 3.8.4; no changes required.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 19 May 2010 10:11:49 +0100

ggcov (0.8.1-1) unstable; urgency=low

  * New upstream release. Closes: #559737, #522432.
  * Move to debhelper v7, quilt format 3.0, Standards-Version 3.8.3
  * Added patches for gcc-4.4 support, build fixes.
  * Enabled 'make check':
   - test015 hangs under make, but runs in runtest. why ?
   - test018 disabled as uses missing gcc functionality.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 08 Jan 2010 21:51:01 +0000

ggcov (0.8-8) unstable; urgency=low

  * Loosen depenency  libdb-dev (>= 4.6.19-1) to (>= 4.6.19), to help backports.
  * Remove duplicate Section: and Priority: elements from debian/control. 
  * Install php stuff into /usr/share/ggcov (wasn't being installed properly).
  * Patch added to add -Wl,--as-needed to link line, remove unnecessary linkages.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 28 Apr 2009 21:39:32 +0100

ggcov (0.8-7) unstable; urgency=low

  * Move to Standards-Version: 3.8.1.0; no changes required.
  * Rebuild for latest bdb. Closes: #5127280.
  * Added Homepage: to debian/control. 

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 31 Mar 2009 00:17:12 +0100

ggcov (0.8-6) unstable; urgency=low

  * Add hurd-i386 to the architectures list. Closes: #482838.
  * Move to Policy : 3.7.3. No changes required.
  * move to debian/compat = 5. No changes required.
  * Fix copyright warning to placate lintian.
  * Move ggcov.desktop to /usr/share/applications from
    /usr/share/gnome/apps, as recommended by lintian.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 27 May 2008 15:43:33 +0100

ggcov (0.8-5) unstable; urgency=low

  * Rebuild with clean environment. Closes: #446340.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 22 Oct 2007 14:37:25 +0100

ggcov (0.8-4) unstable; urgency=medium

  * Rebuild against latest binutils-dev. Closes: #429430. 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 18 Jun 2007 22:25:50 +0100

ggcov (0.8-3) unstable; urgency=low

  * Build against latest Berkeley DB, 4.5. Closes: #421936.
  * Patch from Peter Schueller to work with Debian gcc-3.4. Closes: #407889.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed,  2 May 2007 22:58:11 +0100

ggcov (0.8-2) unstable; urgency=medium

  * Fix FTBFS: estring.C needs va_copy(), not assignment for va_list
    structures. Closes: #399385.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 20 Nov 2006 20:47:43 +0000

ggcov (0.8-1) unstable; urgency=low

  * New upstream release. Closes: #377224.
    - Fixes build issue that meant libglade was linking incorrectly;
     Closes: #376289.
  * Acknowledge NMU with thanks. Closes: #375164.
  * Move to Standards-Version: 3.7.2. No changes required.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 10 Jul 2006 20:20:04 +0100

ggcov (0.6-4.1) unstable; urgency=high

  * Non-maintainer upload.
  * 30_implicit_friend_declaration.patch: Add explicit prototypes for the
    cov_add_* functions, as a friend declaration no longer implicitly declares
    a prototype for the function; fixes FTBFS with GCC 4.1. (Closes: #373837)

 -- Steinar H. Gunderson <sesse@debian.org>  Fri, 23 Jun 2006 21:49:44 +0200

ggcov (0.6-4) unstable; urgency=low

  * Use dh_gconf to place gconf schema in the right place.
    (debhelper >= 4.2.13 to provide this).

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 17 Nov 2005 20:57:58 +0000

ggcov (0.6-3) unstable; urgency=low

  * Build against latest binutils. Closes: #331283. 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon,  3 Oct 2005 20:30:35 +0100

ggcov (0.6-2) unstable; urgency=low

  * Include Build-Depends: libdb4.2-dev. Closes: #302612.
  * Move non-standard directory /usr/var/ggcov to /var/cache/ggcov

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 31 Jul 2005 09:16:18 +0100

ggcov (0.6-1) unstable; urgency=low

  * New upstream version.
  * Move to Standards-Version: 3.6.2. No changes required.
  * Build against g++ 4.0 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 25 Jul 2005 22:02:31 +0100

ggcov (0.3-3) unstable; urgency=low

  * Build on kfreebsd-i386. Closes: #314459. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 18 Jun 2005 13:35:31 +0100

ggcov (0.3-2) unstable; urgency=low

  * Build against sid in the correct environment. Closes: #275737.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu,  4 Nov 2004 20:50:14 +0000

ggcov (0.3-1) unstable; urgency=low

  * New upstream release. Closes: #276519.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue,  2 Nov 2004 06:47:36 +0000

ggcov (0.2.2-6) unstable; urgency=low

  * Build against sid, not experimental. Closes: #275737. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 10 Oct 2004 17:16:31 +0100

ggcov (0.2.2-5) unstable; urgency=low

  * Build fix for gcc-3.4 from  Andreas Jochens. Closes: #273614. 

 -- Alastair McKinstry <mckinstry@debian.org>  Wed,  6 Oct 2004 20:35:46 +0100

ggcov (0.2.2-4) unstable; urgency=low

  * Set Architecture: i386 until properly tested. 
  * Swap binary-arch: and binary-indep: in rules: Closes: #269537.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu,  2 Sep 2004 20:26:59 +0100

ggcov (0.2.2-2) unstable; urgency=low

  * Fix for segfault when given a bad bbg file.
  * Build against sid, not experimental. Closes: #255278.

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 20 Jun 2004 20:18:29 +0100

ggcov (0.2.2-1) unstable; urgency=low

   * Initial Release. Closes: #203055, #206723.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed,  2 June 2004 21:57:53 +0100
